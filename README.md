# Snowcat
### A lightweight, open-source web browser

<img src="images/snowcat.png" alt="Snowcat" width="200"/>

Snowcat is a fast, lightweight web browser built from the ground up with privacy and customizability in mind. It is fully open-source under the GPLv3 license.

## Key Features

-    **Speed**: Snowcat uses a custom browser engine optimized for performance. Pages load blazingly fast.
-    **Lightweight**: The browser has a small memory and disk footprint, perfect for lower powered machines.
-    **Privacy**: No tracking, ads, or data collection. Snowcat doesn't store your history or data without your permission.
-    **Customizable**: Make Snowcat your own with extension support (COMING SOON).
-    **Open source**: Snowcat is fully open source under GPLv3, so you can view and modify the code.

## Getting Started

### Installing

Binaries for Windows, Mac, and Linux are available on our [releases page](https://codeberg.org/snowcat/snowcat/releases).

### Building from source

To build Snowcat from source, see the instructions on our [website](https://snowcat.codeberg.page/download.html).

### Contributing

We welcome contributions! See [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to submit code or bug reports.

## Learn More

-    [Snowcat Homepage](https://snowcat.codeberg.page/)
-    [Forum](https://lemmy.world/c/snowcat)
-    [Official Matrix Space](https://matrix.to/#/%23snowcat:matrix.org)

Copyright 2023 Snowcat Project. Released under GPLv3.
