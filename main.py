import tkinter as tk
from tkinter import *
import sys
import render_page
import sys
import os
import snowcat_globals
from aboutscreen import aboutscreen


if len(sys.argv) == 1:
    page_url = "snowcat://startpage"
else:
    page_url = sys.argv[1]

root = tk.Tk()



def go_back():
    render_page.render_page(snowcat_globals.browser_history.back(1), root, e)

def go_forward():
    render_page.render_page(snowcat_globals.browser_history.forward(1), root, e)

def render_page_from_urlbar(arg=None):
    render_page.render_page(e.get(), root, e)

topbar = Frame(root)
topbar.pack()

back_btn_img = PhotoImage(file="images/back_arrow.png").zoom(2, 2)
back_btn = Button(topbar, text='<', command=go_back, image=back_btn_img, width=30, height=30)
back_btn.pack(side=tk.LEFT)

forward_btn_img = PhotoImage(file="images/forward_arrow.png").zoom(2, 2)
forward_btn = Button(topbar, text='>', command=go_forward, image=forward_btn_img, width=30, height=30)
forward_btn.pack(side=tk.LEFT)

reload_img = PhotoImage(file="images/reload.png").zoom(2, 2)
reload_btn = Button(topbar, command=render_page_from_urlbar, image=reload_img, width=30, height=30)
reload_btn.pack(side=tk.LEFT)

e = Entry(topbar, width=100, font=("Arial 17"))
e.pack(side=tk.LEFT)
e.bind('<Return>', render_page_from_urlbar)

b = Button(topbar, text='Go', command=render_page_from_urlbar, font=("Arial 13"))
b.pack(side=tk.LEFT)

render_page.render_page(page_url, root, e)

if os.name == 'nt':
    root.state('zoomed')
else:
    root.attributes('-zoomed', True)



# create a menubar
menubar = Menu(root)
root.config(menu=menubar)

# create the file_menu
file_menu = Menu(
    menubar,
    tearoff=0
)

# add menu items to the File menu
file_menu.add_command(label='Open...')
file_menu.add_separator()

# add Exit menu item
file_menu.add_command(
    label='Exit',
    command=root.destroy
)

# add the File menu to the menubar
menubar.add_cascade(
    label="File",
    menu=file_menu
)
# create the Help menu
help_menu = Menu(
    menubar,
    tearoff=0
)

help_menu.add_command(
    label='About',
    command=aboutscreen
)

# add the Help menu to the menubar
menubar.add_cascade(
    label="Help",
    menu=help_menu
)


# Launch window
root.mainloop()