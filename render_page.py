import urllib.request
from lxml import html
import lxml
from PIL import Image, ImageDraw, ImageTk, ImageFont
import tkinter as tk
import tkinter
import cssutils
from functools import partial
import esprima
import requests
import sys
import re
from urllib.parse import urljoin
from io import BytesIO
import validators
import os
from custom_gui_classes import *
import snowcat_globals
import pituophis
import validators

def parse_font_family(f):
  fonts = f.split(",")

  myfonts = []

  for i in fonts:
    j = i.strip()

    if j.startswith("\"") and j.endswith("\""):
      j = j[1:-1]

    if not j in unsupported_fonts:
      myfonts.append(j)
  
  fonts = myfonts

  return fonts

invisible_tags = ["style", "script", "head", "body", "title", "meta", "html", "link"]

unsupported_fonts = ["-apple-system", "BlinkMacSystemFont"]

headers = {
    'User-Agent': snowcat_globals.snowcat_config["content"]["headers"]["user_agent"]
}

def render_page(page_url, root, e):
  def get_absolute_link(link):
    global e
    if validators.url(link):
        return link
    else:
        return urljoin(page_url, link)

  
  if page_url.startswith("mailto:"):
    import webbrowser
    webbrowser.open(page_url)


  snowcat_globals.browser_history.visit(page_url)

  e.delete(0,tk.END)
  e.insert(0,page_url)

  page_title = "Snowcat"

  for child in root.winfo_children():
    if type(child) == tkinter.Canvas:
      child.destroy()
    elif type(child) == tkinter.Text:
      child.destroy()

  if page_url == "snowcat://startpage":
    with open("html/startpage.html", "r") as f:
      html_string = f.read()
      for k,v in snowcat_globals.snowcat_config["speed_dial"].items():
        html_string += "<a href=\"" + v + "\">" + k + "</a>"
  elif page_url == "snowcat://about":
    with open("html/about.html", "r") as f:
      html_string = f.read()
  elif page_url.startswith("gopher://"):
    html_string = pituophis.get(page_url).text()
    t = tk.Text(root, width=170, height=100)
    t.insert(tk.END, html_string)
    t["state"] = tk.DISABLED
    t.pack()
  elif not validators.url(page_url) and not page_url.endswith(".com") and not page_url.endswith(".org") and not page_url.endswith(".uk") and not page_url.endswith(".co"):
    render_page(snowcat_globals.snowcat_config["search_engine"] + page_url.replace("search:", ""), root, e)

    return
  else:
    # Fetch HTML page
    try:
      response = requests.get(page_url, headers=headers)
      html_string = response.text

      if response.headers["Content-Type"] == "text/plain":
        html_string = "<html><body><p>" + html_string + "</p></body></html>"
    except Exception as e:
      with open("html/notfound.html", "r") as f:
          html_string = f.read() + "<p>Error: " + str(e) + "</p>"

  css = re.findall("<link rel=\"stylesheet\" href=\".*\">", html_string)
  cssFiles = []

  for c in css:
    t = c.replace("<link rel=\"stylesheet\" href=\"", "").replace("\" type=\"text/css\">", "").replace("\">", "")
    t = urljoin(page_url, t)
    cssFiles.append(t)

  cssString = ""

  for t in cssFiles:
    cssString += requests.get(t).text + "\n"

  style_tags = re.findall("<style>.*<\/style>", html_string, re.DOTALL)
  for i in style_tags:
    cssString += i.replace("\\n", "").replace("\\r", "").replace("<style>", "").replace("</style>", "")

  sheet = cssutils.parseString(cssString)

  # Parse HTML and build DOM tree
  dom = html.fromstring(html_string)

  # Create render tree
  render_tree = []

  # Recursively traverse DOM and add nodes to render tree
  def build_render_tree(node):
      if node.tag is lxml.etree.Comment:
        return

      style = None

      for rule in sheet.cssRules:
        if hasattr(rule, "selectorText"):
          if rule.selectorText == node.tag:
            style = rule.style
            #print(style.getCssText)

      node_render = {
          'node': node,
          'x': 0,
          'y': 0,
          'css': style,
      }

      render_tree.append(node_render)
      
      for child in node.getchildren():
          build_render_tree(child)
          
  build_render_tree(dom)        


  def parse_javascript(js):
    if snowcat_globals.snowcat_config["content"]["javascript"]["enabled"] == False:
      return

    program = esprima.parseScript(js)

    program_body = program.__dict__["body"]

    #print(program_body)

    for l in program_body:
      try:
        if l.__dict__["type"] == "ExpressionStatement":
          if "name" in l.__dict__["expression"].__dict__["callee"].__dict__:
            if l.__dict__["expression"].__dict__["callee"].__dict__["name"] == "alert":
              print("[ALERT]", l.__dict__["expression"].__dict__["arguments"][0].__dict__["value"])
          else:
            if l.__dict__["expression"].__dict__["callee"].__dict__["object"].__dict__["name"] == "console":
              if l.__dict__["expression"].__dict__["callee"].__dict__["property"].__dict__["name"] == "log":
                print("[LOG]", l.__dict__["expression"].__dict__["arguments"][0].__dict__["value"])
              if l.__dict__["expression"].__dict__["callee"].__dict__["property"].__dict__["name"] == "error":
                print("[ERROR]", l.__dict__["expression"].__dict__["arguments"][0].__dict__["value"])
              if l.__dict__["expression"].__dict__["callee"].__dict__["property"].__dict__["name"] == "info":
                print("[INFO]", l.__dict__["expression"].__dict__["arguments"][0].__dict__["value"])
      except:
        print("Error during JS execution")


  # Simple layout to set Y coordinate of each node 
  y = 0
  for node in render_tree:
      if node["node"].tag in invisible_tags:
        node['y'] = 1
      else:
        node['y'] = y
        y += 55

  max_width = max(node['x'] for node in render_tree)
  max_height = max(node['y'] for node in render_tree)

  root.geometry(str(max_width + 1000) + "x655")




  # Create canvas
  canvas = tk.Canvas(root, width=max_width + 1000, height=650)
  canvas.pack(fill="both", expand=True)

  page_frame = tk.Frame(canvas, width=max_width + 1000, height=max_height + 100)
  page_frame.pack()


  # Add scrollbars 
  scrollbar = tk.Scrollbar(canvas, orient="vertical", command=canvas.yview)
  scrollbar.pack(fill="y", side="right")

  canvas.config(yscrollcommand = scrollbar.set) 

  # Put frame in canvas 
  canvas.create_window((0,0), window=page_frame, anchor="nw")

  def on_resize(event):

    # Update scroll region to full canvas size
    canvas.config(scrollregion=canvas.bbox("all"))

    canvas.config(width=event.width, height=event.height)

    page_frame.config(width=event.width)

  canvas.bind('<Configure>', on_resize)

  canvas.configure(scrollregion=canvas.bbox('all'))


  ## SCROLLWHEEL ANYWHERE ON PAGE

  def scroll(event):
    canvas.yview_scroll(int(-1*event.delta/120), "units") 

  root.unbind_all("<MouseWheel>")
  root.bind("<MouseWheel>", scroll)

  ## END SECTION  SCROLLWHEEL ANYWHERE ON PAGE


  def color_config(widget, color, event):
      widget.configure(foreground=color)

  def open_link(link, event):
    page_frame.master.destroy()
    render_page(get_absolute_link(link), root, e)

  for rule in sheet.cssRules:
    if hasattr(rule, "selectorText"):
      if rule.selectorText == "body":
        body_style = rule.style

  if not 'body_style' in locals():
    body_style = {}

  if 'background-color' in body_style:
    page_background = body_style['background-color']
    page_frame.configure(background=body_style['background-color'])
  else:
    page_background = "white"
    page_frame.configure(background='white')

  if 'color' in body_style:
    text_color = body_style['color']
  else:
    text_color = "black"

  # Render nodes
  for node in render_tree:
    
    text = node['node'].text_content()

    text = text.replace("<br>", "\n")

    if not node['node'].getchildren() == []:
      continue

    #font = ImageFont.truetype(node['css']['font-family'],
    #                            node['css']['font-size'])
    
    if node['css'] is not None:
      if 'color' in node['css']:
        fg = node['css']['color']
      else:
        fg = text_color

      if 'background-color' in node['css']:
        bg = node['css']['background-color']
      else:
        bg = page_background

      if 'font-family' in node['css']:
        fonts = parse_font_family(node['css']['font-family'])
      else:
        font = "Arial"
    else:
      # Default Values
      fg = text_color
      bg = page_background
      font = "Arial"

    if 'fonts' in locals():
      if len(fonts) > 0:
        if fonts[0] == "system-ui":
          font = "Arial"
        else:
          font = fonts[0]
      else:
        font = "Arial"

    if node['node'].tag == 'p':
      label = tk.Label(page_frame, text=text, font=(font, 12), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    if node['node'].tag == 'pre':
      label = tk.Label(page_frame, text=text, font=("Consolas", 12), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    if node['node'].tag == 'code':
      label = tk.Label(page_frame, text=text, font=("Consolas", 12), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    if node['node'].tag == 'a':
      label = tk.Label(page_frame, text=text, font=(font, 12), fg="blue", bg=bg)
      label.bind("<Enter>", partial(color_config, label, "#800080"))
      label.bind("<Leave>", partial(color_config, label, "blue"))
      if "href" in node['node'].attrib:
        label.bind("<Button-1>", partial(open_link, node['node'].attrib["href"]))
      label.place(x=node['x'], y=node['y'])

    elif node['node'].tag == 'h1':
      label = tk.Label(page_frame, text=text, font=("Arial", 32), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    elif node['node'].tag == 'h2':
      label = tk.Label(page_frame, text=text, font=("Arial", 24), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    elif node['node'].tag == 'h3':
      label = tk.Label(page_frame, text=text, font=("Arial", 21), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    elif node['node'].tag == 'h4':
      label = tk.Label(page_frame, text=text, font=("Arial", 16), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    elif node['node'].tag == 'h5':
      label = tk.Label(page_frame, text=text, font=("Arial", 13), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    elif node['node'].tag == 'h6':
      label = tk.Label(page_frame, text=text, font=("Arial", 11), fg=fg, bg=bg)
      label.place(x=node['x'], y=node['y'])

    elif node['node'].tag == 'title':
      page_title = text

    elif node['node'].tag == 'input':
      if "type" in node['node'].attrib:
        if node['node'].attrib["type"] == 'text':

          if 'placeholder' in node["node"].attrib:
            input = CustomEntry(page_frame, placeholder=node["node"].attrib['placeholder'], style="TEntry")
          else:
            input = CustomEntry(page_frame, placeholder="", style="TEntry")
          
          input.place(x=node['x'], y=node['y'])
    
        #elif node['node'].attrib["type"] == 'submit':
        #  pass

    elif node['node'].tag == 'script':
      parse_javascript(text)

    elif node['node'].tag == 'img' and "src" in node['node'].attrib:
      if not node['node'].attrib["src"].startswith("data:"):
        if node['node'].attrib["src"].endswith(".png"):

          fd = requests.get(get_absolute_link(node['node'].attrib["src"]))
          image_file = BytesIO(fd.content)
          img = ImageTk.PhotoImage(Image.open(image_file))
          img_label = tk.Label(page_frame, image = img, text="img")
          img_label.image = img
          img_label.place(x=node['x'], y=node['y'])

        elif node['node'].attrib["src"].endswith(".svg"):

          with open("images/nosvg.png", "rb") as f:
            image_file = BytesIO(f.read())
            img = ImageTk.PhotoImage(Image.open(image_file))
            img_label = tk.Label(page_frame, image = img, text="img")
            img_label.image = img
            img_label.place(x=node['x'], y=node['y'])

  root.title(page_title)