import browser_history
import toml

browser_history = browser_history.BrowserHistory("https://example.com")

with open("config.toml", 'r') as f:
    snowcat_config = toml.loads(f.read())